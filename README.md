stomp-pong is websocket server which answers "pong" when "ping" is received.  
stomp-ping is websocket client which connects to stomp-pong and sends to it 
"ping" when "pong" is received from stomp-pong.  
  
To run first run stomp-pong and then stomp-ping with "dev" profile:
`-Dspring.profiles.active=dev`
