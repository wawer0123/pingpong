package com.training.stomp.pingpong.api.message;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

@RequiredArgsConstructor
@Getter
@Builder
public class Greeting implements Message<String> {

    public static final String EVENT = "greeting";

    private final String payload;

    private final MessageHeaders headers;
}
