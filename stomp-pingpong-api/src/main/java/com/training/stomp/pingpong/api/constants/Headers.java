package com.training.stomp.pingpong.api.constants;

import org.springframework.messaging.simp.stomp.StompHeaders;

public class Headers extends StompHeaders {

    public static final String SIMP_DESTINATION = "simpDestination";
    public static final String EVENT = "event";
    public static final String VERSION = "version";
    public static final String PAYLOAD_TYPE = "payloadType";
    public static final String MESSAGE_TYPE = "simpMessageType";
}
