package com.training.stomp.pingpong.api.message;

import com.training.stomp.pingpong.api.event.Ball;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

@RequiredArgsConstructor
@Getter
@Builder
public class PongMessage implements Message<Ball> {

    public static final String EVENT = "pong";

    private final Ball payload;

    private final MessageHeaders headers;
}
