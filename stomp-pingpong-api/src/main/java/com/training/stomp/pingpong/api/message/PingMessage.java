package com.training.stomp.pingpong.api.message;

import com.training.stomp.pingpong.api.event.Ball;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

@RequiredArgsConstructor
@Getter
@Builder
public class PingMessage implements Message<Ball> {

    public static final String EVENT = "ping";

    private final Ball payload;

    private final MessageHeaders headers;
}
