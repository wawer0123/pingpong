package com.training.stomp.ping.core.job;

import com.training.stomp.ping.core.config.AbstractStompBrokerTest;
import com.training.stomp.pingpong.api.constants.Destinations;
import com.training.stomp.pingpong.api.constants.Headers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.Message;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class PingStompJobTest extends AbstractStompBrokerTest {

    @Value("${stomp.channel.pingpong.url}")
    private String url;

    @Autowired
    PingStompJob pingStompJob;

    @BeforeEach
    void setUp() throws IOException {
        super.start(url, Destinations.PINGPONG);
    }

    @Test
    void shouldPing() throws InterruptedException {
        // when
        pingStompJob.run();

        // then
        Message<String> msgReceived1 = super.getBroker().poll(5, TimeUnit.SECONDS);

        assertEquals("ping", msgReceived1.getHeaders().get(Headers.EVENT, List.class).get(0));
    }

}

