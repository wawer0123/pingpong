package com.training.stomp.ping.core.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.training.stomp.ping.core.io.channel.AbstractStompChannel;
import com.training.stomp.ping.core.io.channel.connector.StompConnector;
import com.training.stomp.ping.core.io.channel.handler.message.AbstractStompMessageHandler;
import com.training.stomp.ping.core.io.channel.handler.session.BasicStompSessionHandler;
import com.training.stomp.ping.core.io.channel.subscription.SubscriptionDefinition;
import com.training.stomp.ping.core.io.channel.subscription.SubscriptionDefinitionsBuilder;
import com.training.stomp.ping.core.util.HeadersConverter;
import com.training.stomp.pingpong.api.constants.Headers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class AbstractStompBrokerTest {

    @Autowired
    WebSocketStompClient client;

    @Autowired
    BasicStompSessionHandler basicStompSessionHandler;

    @Autowired
    StompConnector stompConnector;

    @Autowired
    HeadersConverter headersConverter;

    private final BlockingQueue<Message<String>> broker = new LinkedBlockingQueue<>();

    private AbstractStompChannel channel;

    public void start(String url, String topic) throws IOException {
        this.channel = new TestBlockingQueueStompChannel("test", url, topic);
        this.channel.open();
    }

    public BlockingQueue<Message<String>> getBroker() {
        return broker;
    }


    private class TestBlockingQueueStompChannel extends AbstractStompChannel {

        private final String key;
        private final String url;
        private final String topic;

        TestBlockingQueueStompChannel(String key, String url, String topic) {
            super(stompConnector, headersConverter);
            this.key = key;
            this.url = url;
            this.topic = topic;
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public String getUrl() {
            return url;
        }

        @Override
        protected List<SubscriptionDefinition> getSubscriptionDefinitions() {
            return SubscriptionDefinitionsBuilder.init().addSub(topic, new TestBlockingQueueMessageHandler()).build();
        }
    }


    private class TestBlockingQueueMessageHandler extends AbstractStompMessageHandler {

        @Override
        protected boolean test(StompHeaders stompHeaders) {
            return true;
        }

        @Override
        public void accept(StompHeaders stompHeaders, String payload) throws JsonProcessingException {
            Message<String> msg = MessageBuilder.withPayload(payload)
                    .setHeader(Headers.EVENT, stompHeaders.get(Headers.EVENT))
                    .setHeader(Headers.VERSION, stompHeaders.get(Headers.VERSION))
                    .build();
            broker.offer(msg);
        }
    }

}
