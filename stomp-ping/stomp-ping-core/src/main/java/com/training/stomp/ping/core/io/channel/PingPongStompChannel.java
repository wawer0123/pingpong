package com.training.stomp.ping.core.io.channel;

import com.training.stomp.ping.core.io.channel.handler.message.PongStompMessageHandler;
import com.training.stomp.ping.core.io.channel.connector.StompConnector;
import com.training.stomp.ping.core.io.channel.handler.message.LoggingStompMessageHandler;
import com.training.stomp.ping.core.io.channel.subscription.SubscriptionDefinition;
import com.training.stomp.ping.core.io.channel.subscription.SubscriptionDefinitionsBuilder;
import com.training.stomp.ping.core.util.HeadersConverter;
import com.training.stomp.pingpong.api.constants.Destinations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Scope("singleton")
public class PingPongStompChannel extends AbstractStompChannel {

    public static final String KEY = "key";

    @Value("${stomp.channel.pingpong.url}")
    private String url;

    @Autowired
    public PingPongStompChannel(StompConnector stompConnector, HeadersConverter headersConverter) {
        super(stompConnector, headersConverter);
    }

    @Override
    public String getKey() {
        return KEY;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    protected List<SubscriptionDefinition> getSubscriptionDefinitions() {
        return SubscriptionDefinitionsBuilder.init()
                .addSub(Destinations.PINGPONG, new LoggingStompMessageHandler())
                .addSub(Destinations.PINGPONG, new PongStompMessageHandler())
                .build();
    }
}
