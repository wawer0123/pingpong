package com.training.stomp.ping.core;

import com.training.stomp.ping.core.job.PingStompJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
public class AppMain {

    @Autowired
    PingStompJob pingStompJob;

    public static void main(String... args) {
        SpringApplication.run(AppMain.class, args);
    }

    @Profile("dev")
    @Bean
    public CommandLineRunner ping() {
        return args -> {
            ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
            executor.schedule(pingStompJob, 3000, TimeUnit.MILLISECONDS);
        };
    }
}
