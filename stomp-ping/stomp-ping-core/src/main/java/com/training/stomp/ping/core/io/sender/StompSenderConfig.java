package com.training.stomp.ping.core.io.sender;

import com.training.stomp.ping.core.io.channel.PingPongStompChannel;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.messaging.simp.SimpMessagingTemplate;

@Configuration
class StompSenderConfig {

    @Bean
    public Sender pingPongStompSender(
            PingPongStompChannel channel,
            MessageConverter converter
    ) {
        return createSender(channel, converter);
    }

    private StompSender createSender(MessageChannel channel, MessageConverter converter) {
        SimpMessagingTemplate template = new SimpMessagingTemplate(channel);
        template.setMessageConverter(converter);
        return new StompSender(template);
    }
}
