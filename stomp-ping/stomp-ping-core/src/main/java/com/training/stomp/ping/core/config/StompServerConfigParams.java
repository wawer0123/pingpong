package com.training.stomp.ping.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "stomp")
@Data
class StompServerConfigParams {

    private String[] endpoints;
    private String[] topicPrefixes;
    private String[] appPrefixes;
}
