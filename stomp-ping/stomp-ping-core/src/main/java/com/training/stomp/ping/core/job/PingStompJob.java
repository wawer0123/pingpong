package com.training.stomp.ping.core.job;

import com.training.stomp.ping.core.processor.PingProcessor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PingStompJob implements Runnable {

    private final PingProcessor pingProcessor;

    @Override
    public void run() {
        pingProcessor.pingStomp();
    }
}
