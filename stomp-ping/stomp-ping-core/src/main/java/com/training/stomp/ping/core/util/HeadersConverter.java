package com.training.stomp.ping.core.util;

import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.simp.stomp.StompHeaders;

public interface HeadersConverter {

    MessageHeaders toMessageHeaders(StompHeaders stompHeaders);

    StompHeaders toStompHeaders(MessageHeaders messageHeaders);
}
