package com.training.stomp.ping.core.io.channel.handler.message;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.training.stomp.ping.core.exception.StompMessageHandlerException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;

import java.lang.reflect.Type;

@Slf4j
public abstract class AbstractStompMessageHandler implements StompFrameHandler {

    protected abstract boolean test(StompHeaders stompHeaders);

    public abstract void accept(StompHeaders stompHeaders, String payload) throws JsonProcessingException;

    @Override
    public final Type getPayloadType(StompHeaders stompHeaders) {
        return byte[].class;
    }

    @Override
    public void handleFrame(StompHeaders stompHeaders, Object o) {
        if (test(stompHeaders)) {
            try {
                accept(stompHeaders, byteToString(o));
            } catch (Exception e) {
                throw new StompMessageHandlerException(e.getMessage(), e);
            }
        }
    }

    protected String byteToString(Object o) {
        return o instanceof byte[] ? new String((byte[]) o) : o.toString();
    }
}
