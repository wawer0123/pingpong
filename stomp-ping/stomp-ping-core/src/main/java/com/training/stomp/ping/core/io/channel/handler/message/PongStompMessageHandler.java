package com.training.stomp.ping.core.io.channel.handler.message;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.training.stomp.ping.core.io.sender.StompSenders;
import com.training.stomp.pingpong.api.constants.Destinations;
import com.training.stomp.pingpong.api.constants.Headers;
import com.training.stomp.pingpong.api.event.Ball;
import com.training.stomp.pingpong.api.message.PingMessage;
import com.training.stomp.pingpong.api.message.PongMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class PongStompMessageHandler extends AbstractStompMessageHandler {

    @Override
    protected boolean test(StompHeaders stompHeaders) {
        return PongMessage.EVENT.equals(stompHeaders.toSingleValueMap().get(Headers.EVENT));
    }

    @Override
    public void accept(StompHeaders stompHeaders, String payload) throws JsonProcessingException {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            throw new UnsupportedOperationException(e);
        }
        ObjectMapper om = new ObjectMapper();
        Ball ball = om.readValue(payload, Ball.class);

        Message<Ball> pingMsg = MessageBuilder.withPayload(ball)
                .setHeader(Headers.EVENT, PingMessage.EVENT)
                .setHeader(Headers.DESTINATION, Destinations.PINGPONG)
                .build();
        StompSenders.PINGPONG().send(pingMsg);
    }
}
