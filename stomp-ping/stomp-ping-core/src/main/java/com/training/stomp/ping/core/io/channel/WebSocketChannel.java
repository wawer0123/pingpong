package com.training.stomp.ping.core.io.channel;

import org.springframework.messaging.MessageChannel;

import java.io.IOException;
import java.nio.channels.Channel;

public interface WebSocketChannel extends MessageChannel, Channel {

    String getKey();

    String getUrl();

    void open() throws IOException;
}
