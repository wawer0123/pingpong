package com.training.stomp.ping.core.io.channel.connector;

import org.springframework.util.concurrent.ListenableFuture;

public interface WebSocketConnector<T> {

    default ListenableFuture<T> connect(String url) {
        return connect(url, 0);
    }

    ListenableFuture<T> connect(String url, int maxRetries);
}
