package com.training.stomp.ping.core.processor;

import com.training.stomp.ping.core.io.sender.StompSenders;
import com.training.stomp.pingpong.api.constants.Destinations;
import com.training.stomp.pingpong.api.constants.Headers;
import com.training.stomp.pingpong.api.event.Ball;
import com.training.stomp.pingpong.api.message.PingMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PingProcessor {

    public void pingStomp() {
        var ball = new Ball();
        Message<Ball> pingMsg = MessageBuilder.withPayload(ball)
                .setHeader(Headers.DESTINATION, Destinations.PINGPONG)
                .setHeader(Headers.EVENT, PingMessage.EVENT)
                .build();

        StompSenders.PINGPONG().send(pingMsg);
    }
}
