package com.training.stomp.ping.core.io.sender;

import org.springframework.messaging.Message;

import java.util.concurrent.Future;

public interface Sender {

    Future<SendResult> send(Message<?> msg);
}
