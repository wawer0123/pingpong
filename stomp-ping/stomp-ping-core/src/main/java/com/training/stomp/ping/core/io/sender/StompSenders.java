package com.training.stomp.ping.core.io.sender;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class StompSenders implements InitializingBean {

    private final Sender pingPongSender;

    private static Sender staticPingPongSender;

    @Autowired
    public StompSenders(
            @Qualifier("pingPongStompSender") Sender pingPongStompSender
    ) {
        this.pingPongSender = pingPongStompSender;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        this.staticPingPongSender = pingPongSender;
    }

    public static Sender PINGPONG() {
        return staticPingPongSender;
    }
}
