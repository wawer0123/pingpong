package com.training.stomp.pong.core.io.channel.handler.message;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Service;

@Aspect
@Service
@Slf4j
public class MessageHandlerNotifier {

    @Before(value = "execution (* AbstractStompMessageHandler+.handleFrame(..))")
    public void afterHandle(JoinPoint joinPoint) {
        log.info("Handled by [{}]", joinPoint.getTarget());
    }
}
