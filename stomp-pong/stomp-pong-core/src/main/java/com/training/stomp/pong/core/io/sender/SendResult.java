package com.training.stomp.pong.core.io.sender;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.Message;

@RequiredArgsConstructor
@Getter
@Builder
final class SendResult {

    private final boolean isSent;
    private final Message<?> msg;
    private final String destination;
}