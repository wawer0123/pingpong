package com.training.stomp.pong.core.io.listener;

import com.training.stomp.pingpong.api.constants.Destinations;
import com.training.stomp.pingpong.api.constants.Headers;
import com.training.stomp.pingpong.api.message.Greeting;
import com.training.stomp.pong.core.io.sender.StompSenders;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class StompSubscribeListener {

    @EventListener
    public void handleSubscribeEvent(SessionSubscribeEvent event) {
        Message<String> msg = MessageBuilder.withPayload("subscribed")
                .setHeader(Headers.EVENT, Greeting.EVENT)
                .setHeader(Headers.DESTINATION, Destinations.PINGPONG)
                .build();
        StompSenders.PINGPONG().send(msg);
    }

    @EventListener
    public void handleSessionConnectEvent(SessionConnectEvent event) {
        Message<String> msg = MessageBuilder.withPayload("connected")
                .setHeader(Headers.EVENT, Greeting.EVENT)
                .setHeader(Headers.DESTINATION, Destinations.PINGPONG)
                .build();
        StompSenders.PINGPONG().send(msg);
    }
}
