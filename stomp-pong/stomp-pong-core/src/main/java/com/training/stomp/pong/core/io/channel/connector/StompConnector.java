package com.training.stomp.pong.core.io.channel.connector;

import org.springframework.messaging.simp.stomp.StompSession;

public interface StompConnector extends WebSocketConnector<StompSession> {

}
