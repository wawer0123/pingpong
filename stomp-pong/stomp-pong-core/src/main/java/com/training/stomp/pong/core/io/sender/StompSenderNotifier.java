package com.training.stomp.pong.core.io.sender;

import com.training.stomp.pingpong.api.constants.Headers;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

@Aspect
@Service
@Slf4j
public class StompSenderNotifier {

    @AfterReturning(
            pointcut = "execution (* StompSender.send(..))",
            returning = "futureSendResult")
    public void afterSend(JoinPoint joinPoint, Future<SendResult> futureSendResult) {
        logResult(futureSendResult);
    }

    private void logResult(Future<SendResult> futureSendResult) {
        try {
            SendResult sendResult = futureSendResult.get();
            MessageHeaders headers = sendResult.getMsg().getHeaders();
            if (sendResult.isSent()) {
                log.info(">>> [{}] >>> [{}] [{}]",
                        headers.get(Headers.EVENT), headers, sendResult.getMsg().getPayload());
            }
        } catch (Exception e) {
            log.error("Failed sending");
            log.error(e.getMessage(), e);
        }
    }
}
