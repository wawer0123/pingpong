package com.training.stomp.pong.core.processor;

import com.training.stomp.pong.core.io.sender.StompSenders;
import com.training.stomp.pingpong.api.constants.Destinations;
import com.training.stomp.pingpong.api.constants.Headers;
import com.training.stomp.pingpong.api.event.Ball;
import com.training.stomp.pingpong.api.message.PongMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PongProcessor {

    public void pong() {
        Ball ball = new Ball();
        Message<Ball> pongMsg = MessageBuilder.withPayload(ball)
                .setHeader(Headers.DESTINATION, Destinations.PINGPONG)
                .setHeader(Headers.EVENT, PongMessage.EVENT)
                .build();

        StompSenders.PINGPONG().send(pongMsg);

        log.info("pong finished");
    }
}
