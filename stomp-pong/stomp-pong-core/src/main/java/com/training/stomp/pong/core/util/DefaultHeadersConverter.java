package com.training.stomp.pong.core.util;

import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class DefaultHeadersConverter implements HeadersConverter {

    @Override
    public MessageHeaders toMessageHeaders(StompHeaders stompHeaders) {
        Map<String, Object> rawHeaders = stompHeaders.toSingleValueMap().entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        return new MessageHeaders(rawHeaders);
    }

    @Override
    public StompHeaders toStompHeaders(MessageHeaders messageHeaders) {
        StompHeaders stompHeaders = new StompHeaders();
        messageHeaders.entrySet().stream()
                .filter(entry -> !entry.getKey().equals("nativeHeaders"))
                .forEach(entry -> stompHeaders
                        .add(entry.getKey(), entry.getValue() == null ? null : entry.getValue().toString()));
        Map<String, List<String>> nativeHeaders = messageHeaders.get("nativeHeaders", Map.class);
        nativeHeaders.entrySet()
                .forEach(entry -> stompHeaders
                        .addAll(entry.getKey(), entry.getValue() == null ? null : entry.getValue()));
        return stompHeaders;
    }

}
