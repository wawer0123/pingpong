package com.training.stomp.pong.core.io.listener;

import com.training.stomp.pong.core.io.channel.StompChannel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Set;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class StompChannelsListener implements ApplicationListener<ContextRefreshedEvent> {

    private final Set<StompChannel> stompChannels;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        stompChannels.stream()
                .filter(this::isEffective)
                .filter(channel -> !channel.isOpen())
                .forEach(this::reopenChannel);
        log.info("Channels listener has started");
    }

    private boolean isEffective(StompChannel channel) {
        return channel.getKey() != null;
    }

    private void reopenChannel(StompChannel channel) {
        try {
            channel.close();
            channel.open();
        } catch (IOException e) {
            log.error("Listener was unable to reopen the channel [{}] [{}]", channel.getKey(), channel.getUrl());
            log.error(e.getLocalizedMessage(), e);
        }
    }

}
