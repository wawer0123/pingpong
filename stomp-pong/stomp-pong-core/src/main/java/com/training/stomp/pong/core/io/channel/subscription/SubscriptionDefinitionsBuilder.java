package com.training.stomp.pong.core.io.channel.subscription;

import com.training.stomp.pong.core.io.channel.handler.message.AbstractStompMessageHandler;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public final class SubscriptionDefinitionsBuilder {

    private final List<SubscriptionDefinition> subs = new LinkedList<>();

    public static SubscriptionDefinitionsBuilder init() {
        return new SubscriptionDefinitionsBuilder();
    }

    public SubscriptionDefinitionsBuilder addSub(String topic, AbstractStompMessageHandler handler) {
        SubscriptionDefinition sub = SubscriptionDefinition.builder().topic(topic).messageHandler(handler).build();
        subs.add(sub);
        return this;
    }

    public List<SubscriptionDefinition> build() {
        return Collections.unmodifiableList(subs);
    }
}
