package com.training.stomp.pong.core.io.sender;

import com.training.stomp.pingpong.api.constants.Headers;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
class StompSender implements Sender {

    private final SimpMessagingTemplate simpMessagingTemplate;

    @Override
    public Future<SendResult> send(Message<?> msg) {
        return Executors.newSingleThreadExecutor().submit(() -> {
            ValidationResult validationResult = validate(msg);
            return validationResult.isValid() ? doSend(msg) : rejectSending(msg, validationResult.getMessages());
        });
    }

    private SendResult doSend(Message<?> msg) {
        String destination = getHeader(Headers.DESTINATION, msg);
        simpMessagingTemplate.convertAndSend(destination, msg.getPayload(), msg.getHeaders());
        return SendResult.builder().isSent(true).msg(msg).destination(destination).build();
    }

    private SendResult rejectSending(Message<?> msg, List<String> messages) {
        log.error(String.join(", ", messages));
        return SendResult.builder().isSent(false).msg(msg).build();
    }

    private String getHeader(String headerKey, Message<?> msg) {
        return msg.getHeaders().get(headerKey).toString();
    }

    private ValidationResult validate(Message<?> msg) {
        boolean isValid = true;
        List<String> messages = new LinkedList<>();

        String destination = getHeader(Headers.DESTINATION, msg);
        if (destination == null) {
            messages.add(String.format("Not found [%s] header in the message [%s]",
                    Headers.DESTINATION, msg));
            isValid = false;
        }

        return ValidationResult.builder().isValid(isValid).messages(messages).build();
    }

    @Builder
    @Getter
    private static class ValidationResult {
        private final boolean isValid;
        private final List<String> messages;
    }
}
