package com.training.stomp.pong.core.io.sender;

import org.springframework.messaging.Message;

import java.util.concurrent.Future;

public interface Sender {

    Future<SendResult> send(Message<?> msg);
}
