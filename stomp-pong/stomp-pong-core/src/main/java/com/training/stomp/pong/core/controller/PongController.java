package com.training.stomp.pong.core.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class PongController {

//    private final PongProcessor pongProcessor;
//
//    @MessageMapping("/mytopicprefix/ping2")
//    @SendTo("/mytopicprefix/pong2")
//    public Message<Ball> pong(@Payload Ball ball) {
//
//        try {
//            TimeUnit.SECONDS.sleep(2);
//        } catch (InterruptedException e) {
//            throw new UnsupportedOperationException(e);
//        }
//
//        Message<Ball> pongMsg = MessageBuilder.withPayload(ball)
//                .setHeader(Headers.EVENT, PongMessage.EVENT)
//                .setHeader(Headers.DESTINATION, DESTINATIONS.PINGPONG)
//                .build();
//
//        StompSenders.PINGPONG().send(pongMsg);
//
//        return pongMsg;
//    }
//
//    @MessageExceptionHandler
//    @SendTo("/mychannel/errors")
//    public String handleException(Throwable exception) {
//        return exception.getMessage();
//    }
}
