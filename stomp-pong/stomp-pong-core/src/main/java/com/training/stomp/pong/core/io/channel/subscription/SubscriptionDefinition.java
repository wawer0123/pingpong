package com.training.stomp.pong.core.io.channel.subscription;

import com.training.stomp.pong.core.io.channel.handler.message.AbstractStompMessageHandler;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
@Builder
public class SubscriptionDefinition {

    private final String topic;
    private final AbstractStompMessageHandler messageHandler;
}
