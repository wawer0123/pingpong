package com.training.stomp.pong.core.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.server.RequestUpgradeStrategy;
import org.springframework.web.socket.server.standard.TomcatRequestUpgradeStrategy;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

@Configuration
@EnableWebSocketMessageBroker
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class WebSocketServerConfig implements WebSocketMessageBrokerConfigurer {

    private final StompServerConfigParams stompParams;

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.setApplicationDestinationPrefixes(stompParams.getAppPrefixes())
                .enableSimpleBroker(stompParams.getTopicPrefixes());

        log.info("stomp message broker registered on topicPrefixes: [{}]", stompParams.getTopicPrefixes());
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        RequestUpgradeStrategy upgradeStrategy = new TomcatRequestUpgradeStrategy();

        registry.addEndpoint(stompParams.getEndpoints())
                .setHandshakeHandler(new DefaultHandshakeHandler(upgradeStrategy));
        registry.addEndpoint(stompParams.getEndpoints())
                .setAllowedOrigins("*")
                .withSockJS();

        log.info("stomp endpoints registered on endpoints: [{}]", stompParams.getEndpoints());
    }
}
