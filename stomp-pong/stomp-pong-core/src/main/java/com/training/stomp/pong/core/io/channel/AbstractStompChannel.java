package com.training.stomp.pong.core.io.channel;

import com.training.stomp.pong.core.io.channel.connector.StompConnector;
import com.training.stomp.pong.core.io.channel.handler.message.AbstractStompMessageHandler;
import com.training.stomp.pong.core.io.channel.subscription.SubscriptionDefinition;
import com.training.stomp.pong.core.util.HeadersConverter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RequiredArgsConstructor(onConstructor=@__(@Autowired))
@Slf4j
public abstract class AbstractStompChannel implements StompChannel {

    private final int maxRetries = 3;

    private final StompConnector stompConnector;
    private final HeadersConverter headersConverter;

    private StompSession currentStompSession;

    @Override
    public abstract String getKey();

    @Override
    public abstract String getUrl();

    protected abstract List<SubscriptionDefinition> getSubscriptionDefinitions();

    @Override
    public synchronized void open() throws IOException {
        try {
            this.currentStompSession = stompConnector.connect(getUrl(), maxRetries).get();
            getSubscriptionDefinitions()
                    .forEach(subDef -> subscribe(subDef.getTopic(), subDef.getMessageHandler()));

        } catch (InterruptedException | ExecutionException e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    private StompSession.Subscription subscribe(String destination, AbstractStompMessageHandler messageHandler) {
        StompSession.Subscription result = currentStompSession.subscribe(destination, messageHandler);
        log.info("Subscribed to [{}] with subscriptionId: [{}]", destination, result.getSubscriptionId());
        return result;
    }

    @Override
    public synchronized boolean isOpen() {
        return currentStompSession != null  && currentStompSession.isConnected();
    }

    @Override
    public synchronized void close() throws IOException {
        if (currentStompSession != null) {
            currentStompSession.disconnect();
        }
    }

    @Override
    public synchronized boolean send(Message<?> message, long l) {
        if (!isOpen()) {
            try {
                open();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
                return false;
            }
        }
        StompHeaders stompHeaders = headersConverter.toStompHeaders(message.getHeaders());
        currentStompSession.send(stompHeaders, message.getPayload());
        return true;
    }
}
