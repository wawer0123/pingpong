package com.training.stomp.pong.core.io.channel.handler.message;

import com.training.stomp.pingpong.api.constants.Headers;
import com.training.stomp.pingpong.api.message.PingMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class LoggingStompMessageHandler extends AbstractStompMessageHandler {

    @Override
    protected boolean test(StompHeaders stompHeaders) {
        String event = stompHeaders.toSingleValueMap().get(Headers.EVENT);
        return PingMessage.EVENT.equals(event);
    }

    @Override
    public void accept(StompHeaders stompHeaders, String payload) {
        log.info("<<< [{}] <<< [{}] [{}]",
                stompHeaders.toSingleValueMap().get(Headers.EVENT), stompHeaders, payload);
    }
}
