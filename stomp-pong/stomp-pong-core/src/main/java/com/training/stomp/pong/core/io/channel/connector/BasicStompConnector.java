package com.training.stomp.pong.core.io.channel.connector;

import com.training.stomp.pong.core.io.channel.handler.session.AbstractStompSessionHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.FailureCallback;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.SuccessCallback;
import org.springframework.web.socket.messaging.WebSocketStompClient;

@Component
@RequiredArgsConstructor(onConstructor=@__(@Autowired))
@Slf4j
public class BasicStompConnector implements StompConnector {

    private final WebSocketStompClient client;
    private final AbstractStompSessionHandler sessionHandler;

    private int retriedCount;

    @Override
    public ListenableFuture<StompSession> connect(String url, int maxRetry) {
        ListenableFuture<StompSession> connectFuture = client.connect(url, sessionHandler);
        connectFuture.addCallback(onConnectSuccess(url), onConnectFailure(url, maxRetry));
        return connectFuture;
    }

    private SuccessCallback<StompSession> onConnectSuccess(String url) {
        return session -> {
            log.info("Successfully connected to [{}]", url);
        };
    }

    private FailureCallback onConnectFailure(String url, int maxRetry) {
        return session -> {
            log.info("Failed connecting to [{}]", url);
            retryConnect(url, maxRetry);
        };
    }

    private void retryConnect(String url, int maxRetry) {
        if (retriedCount < maxRetry) {
            log.info("Retrying to connect.");
            retriedCount = retriedCount++;
            connect(url, maxRetry);
        }
    }
}
