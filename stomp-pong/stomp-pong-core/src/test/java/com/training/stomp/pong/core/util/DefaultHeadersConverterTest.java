package com.training.stomp.pong.core.util;

import org.junit.jupiter.api.Test;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.simp.stomp.StompHeaders;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DefaultHeadersConverterTest {

    final HeadersConverter headersConverter = new DefaultHeadersConverter();

    final String[] keys = new String[] {"key-0", "keys-1", "keys-2"};
    final String[] values = new String[] {"value-0", "value-1", "value-2"};

    @Test
    void shouldConvertFromStompHeadersToMessageHeaders() {
        // given
        Map<String, Object> rawObjectHeaders = Map.ofEntries(
                Map.entry(keys[0], values[0]),
                Map.entry("nativeHeaders", Map.of(keys[1], List.of(values[1]), keys[2], List.of(values[2])))
        );
        MessageHeaders messageHeaders = new MessageHeaders(rawObjectHeaders);
        Map<String, List<String>> nativeRawHeaders = (Map<String, List<String>>) messageHeaders.get("nativeHeaders");

        // when
        StompHeaders stompHeaders = headersConverter.toStompHeaders(messageHeaders);

        // then
        assertEquals(messageHeaders.get(keys[0]), stompHeaders.toSingleValueMap().get(keys[0]));
        assertEquals(nativeRawHeaders.get(keys[1]).get(0), stompHeaders.toSingleValueMap().get(keys[1]));
        assertEquals(nativeRawHeaders.get(keys[2]).get(0), stompHeaders.toSingleValueMap().get(keys[2]));
    }

    @Test
    void shouldConvertFromMessageHeadersToStompHeaders() {
        // given
        StompHeaders stompHeaders = new StompHeaders();
        stompHeaders.put(keys[0], List.of(values[0]));
        stompHeaders.put(keys[1], List.of(values[1]));

        // when
        MessageHeaders messageHeaders = headersConverter.toMessageHeaders(stompHeaders);

        // then
        assertEquals(stompHeaders.toSingleValueMap().get(keys[0]), messageHeaders.get(keys[0]));
        assertEquals(stompHeaders.toSingleValueMap().get(keys[1]), messageHeaders.get(keys[1]));
    }
}
