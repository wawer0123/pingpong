package com.training.stomp.pong.core.io.channel.handler;

import com.training.stomp.pong.core.io.sender.StompSenders;
import com.training.stomp.pong.core.config.AbstractStompBrokerTest;
import com.training.stomp.pingpong.api.constants.Destinations;
import com.training.stomp.pingpong.api.constants.Headers;
import com.training.stomp.pingpong.api.event.Ball;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class PingPongStompChannelTest extends AbstractStompBrokerTest {

    @Value("${stomp.channel.pingpong.url}")
    private String url;

    @BeforeEach
    void setUp() throws IOException {
        super.start(url, Destinations.PINGPONG);
    }

    @Test
    void shouldPongOnPing() throws InterruptedException {
        // given
        Message<Ball> pingMsg = MessageBuilder
                .withPayload(new Ball())
                .setHeader(Headers.EVENT, "ping")
                .setHeader(Headers.DESTINATION, Destinations.PINGPONG)
                .build();

        // when
        StompSenders.PINGPONG().send(pingMsg);

        // then
        Message<String> msgReceived1 = super.getBroker().poll(5, TimeUnit.SECONDS);
        Message<String> msgReceived2 = super.getBroker().poll(5, TimeUnit.SECONDS);

        assertEquals("ping", msgReceived2.getHeaders().get(Headers.EVENT, List.class).get(0));
    }

}
